package decoration

import (
	"bytes"
	"encoding/gob"
	"fmt"
)

func GetDecorations()[]byte{
	d := &Decoration{		EntityID: 1,
		Xpos:     1,
		Ypos:     1,
		Name:     "forest"}

	buf := &bytes.Buffer{}
	encBin := gob.NewEncoder(buf)

	encBin.Encode(d)
	return buf.Bytes()
}

func AddDecorations(){
	d := &Decoration{		EntityID: 1,
		Xpos:     1,
		Ypos:     1,
		Name:     "forest"}

	buf := &bytes.Buffer{}
	encBin := gob.NewEncoder(buf)

	encBin.Encode(d)

	fmt.Printf("buf: ", encBin.Encode(d))
	fmt.Printf("buf size: %v\n", buf.Len())

	msgFromClient := &Decoration{}
	gob.NewDecoder(buf).Decode(msgFromClient)

	fmt.Printf("msgFromCLient: %v\n", msgFromClient)

}

func DecorationPopulate()[]Decoration{
	dec := &Decoration{		EntityID: 1,
		Xpos:     1,
		Ypos:     1,
		Name:     "forest"}
	decSlice := make([]Decoration, 0)
	for i := 0; i < 5000; i++{
		dec.EntityID++
		dec.Xpos++
		dec.Ypos++
		decSlice = append(decSlice, *dec)
	}
	return decSlice
	//file, _ := json.MarshalIndent(decSlice, "", " ")
	//_ = ioutil.WriteFile("./state.json", file, 0644)
}


