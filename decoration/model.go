package decoration

type Decoration struct {
	EntityID int `json:"entity_id"`
	Xpos     int `json:"x_pos"`
	Ypos     int `json:"y_pos"`
	Name     string `json:"name"`
}
