module rts-game-server

go 1.14

require (
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
)
