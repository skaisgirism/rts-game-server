package connection

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"rts-game-server/decoration"
	"rts-game-server/npc"
	"rts-game-server/resource"
	"strconv"
	"strings"
)

type Message struct {
	ID   string
	Data string
}

func ListenConnection(conn net.Conn){
	//defer conn.Close()
	//go request(conn)
	sendFileToClient(conn)
	remoteAddr := conn.RemoteAddr().String()
	fmt.Println("Client connected from " + remoteAddr)
}

func request(conn net.Conn){
	i := 0
	scanner := bufio.NewScanner(conn)
	for scanner.Scan(){
		ln := scanner.Text()
		if i == 0{
			mux(conn, ln)
		}
		if ln == ""{
		//	headers are done
			break
		}
		i++
	}
}

func mux(conn net.Conn, ln string){
//	request line
	m := strings.Fields(ln)[0] //method
	u := strings.Fields(ln)[1] //uri
	fmt.Println("***METHOD", m)
	fmt.Println("***URI", u)

	//multiplexer
	if m == "GET" && u == "/decorations"{
		sendFileToClient(conn)
	}
}

func sendFileToClient(conn net.Conn){
	fmt.Println("A client has connected!")
	defer conn.Close()
	file, err := os.Open("state.json")
	if err != nil{
		fmt.Println(err)
		return
	}
	fileInfo, err := file.Stat()
	if err != nil {
		fmt.Println(err)
		return
	}
	fileSize := fillString(strconv.FormatInt(fileInfo.Size(), 10), 10)
	fileName := fillString(fileInfo.Name(), 64)
	fmt.Println("Sending filename and filesize!")
	conn.Write([]byte(fileSize))
	conn.Write([]byte(fileName))
	sendBuffer := make([]byte, 1500)
	fmt.Println("Start sending file!")
	for{
		_, err = file.Read(sendBuffer)
		if err == io.EOF{
			break
		}
		conn.Write(sendBuffer)
	}
	fmt.Println("File has been sent, closing connection!")
	return
}

func fillString(returnString string, toLength int) string {
	for {
		lengthString := len(returnString)
		if lengthString < toLength {
			returnString = returnString + ":"
			continue
		}
		break
	}
	return returnString
}

func Populate(){
	dec := decoration.DecorationPopulate()
	res := resource.ResourcePopulate()
	npc := npc.NpcPopulate()
	var state = make([]interface{}, 0)
	state = append(state, dec)
	state = append(state, res)
	state = append(state, npc)

	file, err := json.MarshalIndent(state, "", " ")
	if err != nil{
		log.Fatal(err)
	}
	err = ioutil.WriteFile("./state.json", file, 0644)
	if err != nil{
		log.Fatal(err)
	}
}