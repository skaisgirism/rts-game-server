package resource

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"log"
)

func ResourcePopulate()[]Resource{
	res := &Resource{
		EntityID: 1,
		Xpos:     1,
		Ypos:     3,
		Name:     "Crystal",
		Type:     "Crystal",
		Amount:     1000}
	resSlice := make([]Resource, 0)
	for i := 0; i < 5000; i++{
		res.EntityID++
		res.Xpos++
		res.Ypos++
		resSlice = append(resSlice, *res)
	}
	return resSlice
}

//populating resources on Redis
func resourcePopulateRedis(){
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		log.Fatal(err)
	}

	defer conn.Close()

	for i := 0; i < 5000; i++ {
		_, err = conn.Do("HMSET", "resource:"+ string(i), "entity_id", i, "x_pos", 5, "y_pos", i, "name", "Crystal", "type", "Crystal", "amount", 1000)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(i)
	}
	fmt.Println("success")
}