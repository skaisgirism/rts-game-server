package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"log"
	"net"
	"rts-game-server/connection"
)

var client *redis.Client

func main(){
	//connection.Populate()
	//Creating TCP listener
	ln, err := net.Listen("tcp", ":9000")
	if err != nil {
		panic(err)
	}
	defer ln.Close()
	fmt.Println("TCP listening on port :9000")

	// listening for incoming connections
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println("Connection error: ", err)
		}else{
			fmt.Println("new connection")
		}

		// listen to connections in another goroutine
		go connection.ListenConnection(conn)
	}
	//decoration.Populate()
	//decoration.AddDecorations()

}
