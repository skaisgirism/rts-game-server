package npc

type Npc struct {
	EntityID int    `json:"entity_id"`
	Xpos     int    `json:"x_pos"`
	Ypos     int    `json:"y_pos"`
	Name     string `json:"name"`
	Level    int `json:"level"`
	Gold     int `json:"gold"`
	Crystal  int `json:"crystal"`
	Metal    int `json:"metal"`
}
