package npc

func NpcPopulate()[]Npc {
	npc := &Npc{
		EntityID: 1,
		Xpos:     6,
		Ypos:     1,
		Name:     "Crystal",
		Level:    1,
		Gold:     500,
		Crystal:  500,
		Metal:    500}
	npcSlice := make([]Npc, 0)
	for i := 0; i < 5000; i++ {
		npc.EntityID++
		npc.Xpos++
		npc.Ypos++
		npcSlice = append(npcSlice, *npc)
	}
	return npcSlice
}
